
//  Теоретичні питання
//  1. Які існують типи даних у Javascript?
// Всього існує 8 типів даних: number, bigint, string, 'Boolean', null, undefined, object, symbol.


//  2. У чому різниця між == і ===?
// Оператор == перетворює операнди різних типів до числа, а === перевіряє рівність без приведення типів.


// 3. Що таке оператор?
// Оператор привласнення або будь-який інший можна уявити собі як деяку вбудовану функцію JavaScript в даному випадку з ім'ям =, 
// яка виконує деякі дії і повертає якийсь результат.



const userName = prompt("Enter your name");
while (!userName) {
    userName = prompt("Enter your name");
}
const userAge = prompt("Enter your age");
while (!userAge || isNaN(userAge)) {
    userAge = prompt("Enter your age");
} if (userAge < 18) {
    alert("You are not allowed to visit this website.");
} else if (+userAge >= 18 && +userAge <= 22) {
    const confirmation = confirm("Are you sure you want to continue?");
    if (confirmation) {
        alert(`Welcome, ${userName}`);
    }else {
        alert("You are not allowed to visit this website.");
    }
} if (+userAge > 22) {
    alert(`Welcome, ${userName}`);
}

